
var express = require("express");
var bodyParser = require("body-parser");

var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/sigma');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("Mongo DB Sucessfully connected.");
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

app.get('/', function(req, res) {
    res.json({"message": "Sigma Server is running"})
});


require('./src/server/user/user.routes.js')(app);
require('./src/server/store/store.routes.js')(app);
require('./src/server/product/product.routes.js')(app);
require('./src/server/custsupp/custsupp.routes.js')(app);
require('./src/server/chartofacc/chartofacc.routes.js')(app);
require('./src/server/city/city.routes.js')(app);
require('./src/server/salesarea/salesarea.routes.js')(app);
require('./src/server/unit/unit.routes.js')(app);
require('./src/server/partyattribute1/partyattribute1.routes.js')(app);
require('./src/server/partyattribute2/partyattribute2.routes.js')(app);
require('./src/server/partyattribute3/partyattribute3.routes.js')(app);
require('./src/server/saleman/saleman.routes.js')(app);
require('./src/server/salemanattach/salemanattach.routes.js')(app);
require('./src/server/itemattribute1/itemattribute1.routes.js')(app);
require('./src/server/itemattribute2/itemattribute2.routes.js')(app);
require('./src/server/itemattribute3/itemattribute3.routes.js')(app);

app.listen(8080, function(){
    console.log("Sigma Server is running.")
});




