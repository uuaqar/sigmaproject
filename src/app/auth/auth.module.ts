import { NgModule } from '@angular/core';

import { LoginComponent } from './login.component';

import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule } from '@angular/forms';
import { RESTService } from 'app/shared/rest.service';
import { Globals } from 'app/vo/globals';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';  

@NgModule({
  imports: [ AuthRoutingModule,FormsModule, 
    ModalModule.forRoot(),
    CommonModule, 
  ],
    
  declarations: [
    LoginComponent
  ],
  providers:[
    RESTService,
    Globals
  ]
})
export class AuthModule { }
