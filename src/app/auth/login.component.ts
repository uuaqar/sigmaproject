import { Component, ViewChild } from '@angular/core';
import { RESTService } from '../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';

@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  usr_name:string = '';
  usr_id:string = '';
  usr_pwd:string = '';
  usr_newpwd:string = '';
  users:any;
  superusers = new Array();
  updateUsers = new Array();
  obj:any;
  // public primaryModal;

  @ViewChild('primaryModal') changePasswordModal: any;

  constructor(private service:RESTService,
    private route: ActivatedRoute,
    private router: Router) { 

      this.updateUsers = [{'id':'', 'name': '', 'password':'', 'isactive':true, 'address':''}]
      
    }

    login(){
    
    if(this.usr_id == '' || this.usr_id == null)
    {
      alert('Enter Username');
      return;
    }
    if(this.usr_pwd == '' || this.usr_pwd == null)
    {
      alert('Enter Password');
      return;
    }
        
    this.service.getLogin(this.usr_id.toString(),this.usr_pwd.toString())
    .subscribe( 
      data => {
        this.users = data

        if (this.users.length == 0) {
          
          if ((this.usr_id).toUpperCase() === 'SUPER' && (this.usr_pwd).toUpperCase() === 'SUPER') {
            
            this.superusers = [{'id':'super', 'name': 'Administrator', 'password':'super', 'isactive':true, 'firstlogin':false }]   
            
            this.service.saveData('adduser',this.superusers)
            .subscribe( 
              superdata => {
                
                if (superdata.errmsg != '' && superdata.errmsg != undefined ) {
                  alert(superdata.errmsg)
                }else
                {
                  
                }  
              } 
            );

            localStorage.setItem('loggedin','1');
            localStorage.setItem('usrname',this.superusers[0].name.toString());
            this.router.navigateByUrl('/pages/dashboard');
            
        } else {
            alert(`Invalid User ID / Password.`);
        }

        }  else {
          for (let i = 0; i<this.users.length; i++ ){
            if (this.usr_id === this.users[i].id && this.usr_pwd === this.users[i].password ) {
              if (this.users[i].isactive !== true) {
                alert(`User is inactive.`);
              } 

              else if (this.usr_id === this.users[i].id && this.usr_pwd === this.users[i].password &&
                this.users[i].firstlogin == true ){
                  this.changePasswordModal.show()
              }
              
              else if (this.usr_id === this.users[i].id && this.usr_pwd === this.users[i].password &&
                this.users[i].isactive === true ){
                localStorage.setItem('loggedin','1');
                localStorage.setItem('usrname',this.users[i].name.toString());
                this.router.navigateByUrl('/pages/dashboard');
  
                // alert(`User ${this.users[i].name} loggedin successfuly.`);
              }  
            } 
          }
        }
      } 
    );


  };

  validatePassword(){
    if (this.usr_newpwd != '' && this.usr_newpwd != null ){
        this.updatePassword();
    } else  {
      alert(`Password cannot be empty.`);
    }
    
  }

  updatePassword(){
    this.updateUsers[0].id = this.users[0].id
    this.updateUsers[0].name = this.users[0].name
    this.updateUsers[0].password = this.usr_newpwd;
    this.updateUsers[0].isactive = this.users[0].isactive
    this.updateUsers[0].address = this.users[0].address
    this.updateUsers[0].firstlogin = false

    this.service.updateData('user',this.users[0].id, this.updateUsers, )
    .subscribe( 
      dataupdate => {
        if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
          alert(dataupdate.errmsg)
        }else
        {
          this.changePasswordModal.hide()
          alert(`Password changed successfully. Please re-login.`);
        }  
      } 
    );
  }
};




