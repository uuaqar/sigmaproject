import { NgModule } from '@angular/core';
import { EvaluationComponent } from './evaluation.component'
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';
// Components Routing
import { ManagementRoutingModule } from './management-routing.module';

@NgModule({
  imports: [
    ManagementRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule
  ],
  declarations: [
    EvaluationComponent
  ]
})
export class ManagementModule { }
