import { Observable } from 'rxjs/Observable';
import { Injectable, Inject} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Globals } from '../vo/globals';
import { ExtraOptions } from '@angular/router';

@Injectable()
export class RESTService {

  uid : String = '';
  ukey : String = '';
  data : string = '';
  fullurl : string = '';
  prnUrl:string = '';
  list:Observable<any>;
  res:Observable<any>;
  resdel:Observable<any>;
  params:Observable<any>;

  constructor(@Inject(Http) private http: Http,private golbal:Globals) {
  }

getLogin(loginname:string,pwd:string):Observable<any>{
  this.fullurl = this.golbal.weburl + 'login';
  this.res = this.http.get(`${this.fullurl}/${loginname}/${pwd}`)
  .map((result: Response) => result.json())
  .catch(this.getError);
  return this.res;
}

getDatawith1param(url:string,data:string): Observable<any> {
    this.uid = localStorage.getItem('usrid');
    this.ukey = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    this.list = this.http.get(`${this.fullurl}/${data}`)
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.list;
  }

  getData(url:string): Observable<any> {
    this.uid = localStorage.getItem('usrid');
    this.ukey = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    this.list = this.http.get(`${this.fullurl}/`)
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.list;
  }

  getMax(url:string): Observable<any> {
    this.uid = localStorage.getItem('usrid');
    this.ukey = localStorage.getItem('usrkey');
    this.fullurl = `${this.golbal.weburl}max${url}`;
    this.list = this.http.get(`${this.fullurl}/`)
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.list;
  }

  getMaxWithParam(url:string, data:string): Observable<any> {
    this.uid = localStorage.getItem('usrid');
    this.ukey = localStorage.getItem('usrkey');
    this.fullurl = `${this.golbal.weburl}max${url}`;
    this.list = this.http.get(`${this.fullurl}/${data}`)
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.list;
  }

  findData(url:string,id:string): Observable<any> {
    this.uid = localStorage.getItem('usrid');
    this.ukey = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    this.list = this.http.get(`${this.fullurl}/${id}`)
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.list;
  }

  saveData(url:string,data:any): Observable<any> {
    const usrid : String= localStorage.getItem('usrid');
    const usrkey : String = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    let headers = new Headers({'Content-Type': 'application/json'});
    this.res = this.http.post(`${this.fullurl}`,data[0], new RequestOptions({headers}))
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.res;
  }

  updateData(url:string,id:string,data:any): Observable<any> {
    const usrid : String= localStorage.getItem('usrid');
    const usrkey : String = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    let headers = new Headers({'Content-Type': 'application/json'});
    this.res = this.http.put(`${this.fullurl}/${id}`,data[0], new RequestOptions({headers}))
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.res;
  }

  updateDatawithparam(url:string,id:string,type:string,data:any): Observable<any> {
    const usrid : String= localStorage.getItem('usrid');
    const usrkey : String = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    let headers = new Headers({'Content-Type': 'application/json'});
    this.res = this.http.put(`${this.fullurl}/${id}/${type}`,data[0], new RequestOptions({headers}))
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.res;
  }

  deletedata(url:string,id:string): Observable<any> {
    const usrid : String= localStorage.getItem('usrid');
    const usrkey : String = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    let headers = new Headers({'Content-Type': 'application/json'});
    // this.data = JSON.stringify({usrid,usrkey,data});
    this.resdel = this.http.delete(`${this.fullurl}/${id}`)
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.resdel;
  }

  deletedatawithparam(url:string,id:string, type:string): Observable<any> {
    const usrid : String= localStorage.getItem('usrid');
    const usrkey : String = localStorage.getItem('usrkey');
    this.fullurl = this.golbal.weburl + url;
    let headers = new Headers({'Content-Type': 'application/json'});
    // this.data = JSON.stringify({usrid,usrkey,data});
    this.resdel = this.http.delete(`${this.fullurl}/${id}/${type}`)
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.resdel;
  }
  
  getDatawithmultipleparams(url:string,data:any): Observable<any> {
    this.fullurl = this.golbal.weburl + url;
    let headers = new Headers({'Content-Type': 'application/json'});
    this.res = this.http.post(`${this.fullurl}/`,data, new RequestOptions({headers}))
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.res;
  }

  private getError(error: Response): Observable<any> {
      return Observable.throw(error.toString() || 'Server Issue');
  }

  private getHeaders():Headers {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  // Retrieve report list
  getPrint(ReportName:string,param:any,paramVal:any): Observable<any> {
    const usrid : String= localStorage.getItem('usrid');
    const usrkey : String = localStorage.getItem('usrkey');
    this.prnUrl = this.golbal.weburl + 'reports/prn';
    this.data = JSON.stringify({usrid,usrkey,ReportName,param,paramVal});

    console.log('Report Printing');
    this.res = this.http.post(`${this.prnUrl}/`, this.data )
    .map((result: Response) => result.json())
    .catch(this.getError);
    return this.res;

  }
}
