import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { error } from 'util';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-saleman',
  templateUrl: './saleman.component.html',
})
export class SalemanComponent  {

  sm_name:string = '';
  sm_code:string = '';
  sm_isActive:string = '1';
  sm_address:string = '';
  sm_cnic:string = '';
  sm_cell:string = '';
  max_id:string = '0';
  salemans = new Array();
  rows:any;
  find_rows:any;
  row:any;  
  MaxNo:any;
  result:any;
  selected = [];
  parent_selected = [];
  temp = [];
  // public primaryModal;

  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;
  parent:boolean = false;
  parentselect:boolean = false;
  open:boolean = true;

  @ViewChild(DatatableComponent) primaryTable: DatatableComponent;
  @ViewChild('primaryModal') parentModal: any;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager) { 
        
      this.getdata();
      this.getMax();
      this.enabledisable(true);
      this.salemans = [{'code':'', 'name': '', 'isactive':'', 'address': '', 'cnic': '', 'cell': '' }]
    }
    
  columns = [
    { name: 'Code' },
    { name: 'Name' },
    { name: 'Active'},
    { name: 'Cell'}
  ];

  
  getMax(){
    this.service.getMax('saleman')
    .subscribe( 
      data => {
        this.MaxNo = data
        if (data.length === 0 ){
          this.max_id = '1'
        } else {
          this.max_id = String(Number(this.MaxNo[0]._id) + 1);
        }
        this.sm_code = this.max_id;
        } 
    );
  }

  save(){
    
    if (this.sm_code == '' ){
        this.showError('Code cannot be empty.')
        return
    }

    if (this.sm_name == '' ){
      this.showError('Name cannot be empty.')
      return
    }
    
    this.salemans[0].code = this.max_id;
    this.salemans[0].name = this.sm_name;
    this.salemans[0].isactive = this.sm_isActive;
    this.salemans[0].address = this.sm_address;
    this.salemans[0].cnic = this.sm_cnic;
    this.salemans[0].cell = this.sm_cell;

     this.result = this.service.saveData('addsaleman',this.salemans)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.sm_code = '';
            this.sm_name = '';
            this.sm_isActive = '1';
            this.sm_address = '';
            this.sm_cnic = '';
            this.sm_cell = '';
            
          }
        },
      );

      this.list = true
      this.entry = false
    }  

    getdata(){
      this.service.getData('saleman')
      .subscribe( 
        data => {
          this.rows = data
          // this.selected = [data[0]];
          // this.onSelect(data[0]);
          
          this.enabledisable(true)
          // this.refresh();

          this.temp = [...data];
          } 
      );
    
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.sm_code == '' ){
          this.showError('Invalid Code.')
          return
        }
    
        this.service.deletedata('saleman',this.sm_code.toString())
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.sm_code = '';
              this.sm_name = '';
              this.sm_isActive = '1';
              this.sm_address = '';
              this.sm_cnic = '';
              this.sm_cell = '';
            }
          } 
        );
      }
    
    }

    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ||
                ( d.cell.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.primaryTable.offset = 0;
    }

    update(){

      if (this.sm_code == '' ){
        this.showError('Code cannot be empty.')
        return
      }
  
      if (this.sm_name == '' ){
        this.showError('Name cannot be empty.')
        return
      }
      
      this.salemans[0].code = this.sm_code;
      this.salemans[0].name = this.sm_name;
      this.salemans[0].isactive = this.sm_isActive;
      this.salemans[0].address = this.sm_address;
      this.salemans[0].cnic = this.sm_cnic;
      this.salemans[0].cell = this.sm_cell;

      this.service.updateData('saleman',this.sm_code.toString(), this.salemans, )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess('Record updated.'); 
            this.getdata();
          }  
        } 
      );

      this.selected = [];
      this.list = true
      this.entry = false
    }

    refresh(){

      this.list = false
      this.entry = true
      this.saveshow = true

      this.getMax();
      // this.sm_code = '';
      this.sm_name = '';
      this.sm_isActive = '1';
      this.sm_address = '';
      this.sm_cnic = '';
      this.sm_cell = '';
      // this.enabledisable(true)
      this.selected = [];
      
    }

    onSelect({ selected }) {

      this.sm_code = this.selected[0].code;
      this.sm_name = this.selected[0].name;
      this.sm_isActive = this.selected[0].isactive;
      this.sm_address = this.selected[0].address;
      this.sm_cnic = this.selected[0].cnic;
      this.sm_cell = this.selected[0].cell;


      // this.sm_address = this.selected[0].parent;      
      this.enabledisable(false)
    }

    enabledisable(action:boolean){
      this.newbutton = action;
      this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

}
