import { Component, OnInit, ViewContainerRef, ViewChild, } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-chartofacc',
  templateUrl: './chartofacc.component.html',
})
export class ChartOfAccComponent  {
  coa_title:string = '';
  coa_code:string = '';
  coa_isActive:boolean = false;
  coa_nature:string = '';
  coa_type:string = '1';
  max_id:string = '0';
  COAs = new Array();
  rows:any;
  MaxNo:any;
  row:any;
  result:any;
  selected = [];
  temp = [];

  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager) { 
        
      this.getdata();
      this.getMax();
      this.enabledisable(true);
      this.COAs = [{'code':'', 'title': '', 'isactive':true, 'type':'1', 'nature':'1' }]
    
    }
    
  columns = [
    { name: 'Code', width: 50 },
    { name: 'Title' },
    { name: 'Active'}
  ];

  getMax(){
    this.service.getMax('chartofacc')
    .subscribe( 
      data => {
        this.MaxNo = data
        if (data.length === 0 ){
          this.max_id = '1'
        } else {
          this.max_id = String(Number(this.MaxNo[0].code) + 1);
        }
        this.coa_code = this.max_id;
        } 
    );
  }
  
  save(){
    
    if (this.coa_code == '' ){
        this.showError('Code cannot be empty.')
        return
    }

    if (this.coa_title == '' ){
      this.showError('Title cannot be empty.')
      return
    }
    
    this.COAs[0].code = this.max_id;
    this.COAs[0].title = this.coa_title;
    this.COAs[0].isactive = this.coa_isActive;
    this.COAs[0].type = this.coa_type;
    this.COAs[0].nature = this.coa_nature;
        
     this.result = this.service.saveData('addchartofacc',this.COAs)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.coa_code = '';
            this.coa_title = '';
            this.coa_isActive = true;
            this.coa_type = '';
            this.coa_nature = '';
          }
         
        },
        
      );

      this.list = true
      this.entry = false

    }  

    getdata(){
      this.service.getData('chartofacc' )
      .subscribe( 
        data => {
          this.rows = data
          // this.selected = [data[0]];
          // this.onSelect(data[0]);
          
          this.enabledisable(true)
          // this.refresh();
          this.temp = [...data];
          } 
      );
    
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.coa_code == '' ){
          this.showError('Invalid Code.')
          return
        }
    
        this.service.deletedata('chartofacc',this.coa_code)
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.coa_code = '';
              this.coa_title = '';
              this.coa_isActive = true
              this.coa_type = '';
              this.coa_nature = '';
            }
          } 
        );
      }
    
    }

    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.title.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.table.offset = 0;
    }

    update(){

      if (this.coa_code == '' ){
        this.showError('Code cannot be empty.')
        return
      }
  
      if (this.coa_title == '' ){
        this.showError('Title cannot be empty.')
        return
      }
      
      this.COAs[0].code = this.coa_code;
      this.COAs[0].title = this.coa_title;
      this.COAs[0].isactive = this.coa_isActive;
      this.COAs[0].type = this.coa_type;
      this.COAs[0].nature = this.coa_nature;

      this.service.updateData('chartofacc',this.coa_code, this.COAs )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess('Record updated.'); 
            this.getdata();
          }  
        } 
      );
      this.selected = [];
      this.list = true
      this.entry = false
    }

    refresh(){

      this.list = false
      this.entry = true
      this.saveshow = true

      this.getMax();
      //this.coa_code = '';
      this.coa_title = '';
      this.coa_isActive = true;
      this.coa_type = '1';
      this.coa_nature = '1';
      this.enabledisable(true)
      this.selected = [];
    }
    
    checkactive(){
      this.coa_isActive = !this.coa_isActive
    }

    onSelect({ selected }) {

      this.coa_code = this.selected[0].code;
      this.coa_title = this.selected[0].title;
      this.coa_isActive = this.selected[0].isactive;
      this.coa_type = this.selected[0].type;
      this.coa_nature = this.selected[0].nature;
      this.enabledisable(false)

    }
  
    enabledisable(action:boolean){
      this.newbutton = action;
      this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

    
  
}
