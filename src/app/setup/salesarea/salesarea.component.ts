import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { error } from 'util';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-salesarea',
  templateUrl: './salesarea.component.html',
})
export class SalesareaComponent  {
  parentData = [];

  sa_name:string = '';
  sa_code:string = '';
  sa_type:string = '1';
  sa_parent:string = '';
  sa_parent_name:string = '';
  max_id:string = '0';
  salesareas = new Array();
  rows:any;
  sa_parent_rows:any;
  find_rows:any;
  row:any;  
  MaxNo:any;
  result:any;
  selected = [];
  parent_selected = [];
  temp = [];
  // public primaryModal;

  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;
  parent:boolean = false;
  parentselect:boolean = false;
  open:boolean = true;

  @ViewChild(DatatableComponent) primaryTable: DatatableComponent;
  @ViewChild(DatatableComponent) parentTable: DatatableComponent;

  @ViewChild('primaryModal') parentModal: any;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager) { 
        
      this.getdata();
      this.getMax();
      this.getParentData();
      this.enabledisable(true);
      this.salesareas = [{'code':'', 'name': '', 'type':'', 'parent': '' }]
    }
    
  columns = [
    { name: 'Code' },
    { name: 'Name' },
    { name: 'Type'},
    { name: 'Parent'}
  ];

  getMax(){
    this.service.getMax('salesarea')
    .subscribe( 
      data => {
        this.MaxNo = data
        if (data.length === 0 ){
          this.max_id = '1'
        } else {
          this.max_id = String(Number(this.MaxNo[0]._id) + 1);
        }
        this.sa_code = this.max_id;
        } 
    );
  }

  sa_parent_columns = [
    { id: 'sa_parent_code', name: 'Code' },
    { id: 'sa_parent_name', name: 'Name' }
  ];

  save(){
    
    if (this.sa_code == '' ){
        this.showError('Code cannot be empty.')
        return
    }

    if (this.sa_name == '' ){
      this.showError('Name cannot be empty.')
      return
    }
    
    this.salesareas[0].code = this.max_id;
    this.salesareas[0].name = this.sa_name;
    this.salesareas[0].type = this.sa_type;
    this.salesareas[0].parent = this.sa_parent;

     this.result = this.service.saveData('addsalesarea',this.salesareas)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.sa_code = '';
            this.sa_name = '';
            this.sa_type = '1';
            this.sa_parent = '';
            this.sa_parent_name = '';
            this.parent_selected = []
          }
        },
      );

      this.list = true
      this.entry = false
    }  

    getdata(){
      this.service.getData('salesarea')
      .subscribe( 
        data => {
          this.rows = data
          // this.selected = [data[0]];
          // this.onSelect(data[0]);
          
          this.enabledisable(true)
          this.getParentData();
          // this.refresh();

          this.temp = [...data];
          } 
      );
    
    }

    getParentData(){
      this.service.getData('parentsalesarea')
      .subscribe( 
        data_parent => {
          this.sa_parent_rows = data_parent

          this.parentData = [...data_parent];
        } 
      );
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.sa_code == '' ){
          this.showError('Invalid Code.')
          return
        }
    
        this.service.deletedata('salesarea',this.sa_code.toString())
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.sa_code = '';
              this.sa_name = '';
              this.sa_type = '1';
              this.sa_parent = '';
              this.sa_parent_name = '';
              this.parent_selected = []
            }
          } 
        );
      }
    
    }

    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.primaryTable.offset = 0;
    }

    updateParentFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.parentData.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.sa_parent_rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.parentTable.offset = 0;
    }

    update(){

      if (this.sa_code == '' ){
        this.showError('Code cannot be empty.')
        return
      }
  
      if (this.sa_name == '' ){
        this.showError('Name cannot be empty.')
        return
      }
      
      this.salesareas[0].code = this.sa_code;
      this.salesareas[0].name = this.sa_name;
      this.salesareas[0].type = this.sa_type;
      this.salesareas[0].parent = this.sa_parent;
      this.service.updateData('salesarea',this.sa_code.toString(), this.salesareas, )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess('Record updated.'); 
            this.getdata();
          }  
        } 
      );

      this.selected = [];
      this.parent_selected = []
      this.list = true
      this.entry = false

      this.parentselect = false
      this.open = true

    }

    refresh(){

      this.list = false
      this.entry = true
      this.saveshow = true

      this.getMax();
      // this.sa_code = '';
      this.sa_name = '';
      this.sa_type = '1';
      this.sa_parent = '';
      this.sa_parent_name = '';
      // this.enabledisable(true)
      this.selected = [];
      this.parent_selected = []
    }
      
    getSalesAreaDesc(sa_code: string){

      if(sa_code) {
        this.service.findData('salesarea', sa_code)
        .subscribe(
          data_find => {
            this.find_rows = data_find
            this.setSalesAreaDesc()
          } 
        );
      }
    }

    setSalesAreaDesc(){
      this.sa_parent_name = this.find_rows[0].name;
    }

    onSelect({ selected }) {

      this.sa_code = this.selected[0].code;
      this.sa_name = this.selected[0].name;
      this.sa_type = this.selected[0].type;
      // this.sa_parent = this.selected[0].parent;

      if (this.sa_type == '1') {
        this.parent = true
        this.sa_parent = ''
        this.sa_parent_name = ''
      }
      else{
        this.parent = false
        this.sa_parent = this.selected[0].parent;
        this.getSalesAreaDesc(this.sa_parent);
      }
      
      this.enabledisable(false)
    }

    onSelectParent({ parent_selected }){
      
      this.sa_parent = this.parent_selected[0].code;
      this.sa_parent_name = this.parent_selected[0].name;
      this.parentModal.hide()

    }

    showparent(){
      this.parentselect = true
      this.open = false
    }
    
    closeparent(){
      this.parentselect = false
      this.open = true
    }

    enabledisable(action:boolean){
      this.newbutton = action;
      this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

}
