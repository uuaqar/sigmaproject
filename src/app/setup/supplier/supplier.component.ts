import { Component, OnInit, ViewContainerRef, ViewChild, } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { error } from 'util';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-supplier',
  templateUrl: 'supplier.component.html',

  })
  
export class SupplierComponent {

  supp_name:string = '';
  supp_code:string = '';
  supp_isActive:boolean = false;
  supp_address:string = '';
  supp_contact:string = '';
  supp_type:string = '2';
  max_id:string = '0';
  suppliers = new Array();
  rows:any;
  MaxNo:any;
  row:any;
  result:any;
  selected = [];
  temp = [];

  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager) { 
        
      this.getdata();
      this.getMax();
      this.enabledisable(true);
      this.suppliers = [{'code':'', 'name': '', 'type':'2', 'isactive':true, 'address':'','contact':'' }]
    
    }
    
  columns = [
    { name: 'Code', width: 50 },
    { name: 'Name' },
    { name: 'Active'},
    { name: 'Address'},
    { name: 'Contact'}
  ];

  getMax(){
    this.service.getMaxWithParam('custsupp', this.supp_type)
    .subscribe( 
      data => {
        this.MaxNo = data
        if (data.length === 0 ){
          this.max_id = '1'
        } else {
          this.max_id = String(Number(this.MaxNo[0].code) + 1);
        }
        this.supp_code = this.max_id;
        } 
    );
  }

  save(){
    
    if (this.supp_code == '' || this.supp_code == '0' ){
        this.showError('Code cannot be empty or Zero.')
        return
    }

    if (this.supp_name == '' ){
      this.showError('Name cannot be empty.')
      return
    }
    
    this.suppliers[0].code = this.max_id;
    this.suppliers[0].name = this.supp_name;
    this.suppliers[0].isactive = this.supp_isActive;
    this.suppliers[0].address = this.supp_address;
    this.suppliers[0].contact = this.supp_contact;
        
     this.result = this.service.saveData('addcustsupp',this.suppliers)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.supp_code = '';
            this.supp_name = '';
            this.supp_isActive = true;
            this.supp_address = '';
            this.supp_contact = '';
          }
         
        },
      );

      this.list = true
      this.entry = false

    }  

    getdata(){
      this.service.getDatawith1param('custsupp', this.supp_type )
      .subscribe( 
        data => {
          this.rows = data
          // this.selected = [data[0]];
          // this.onSelect(data[0]);
          this.enabledisable(true)
          // this.refresh();
          this.temp = [...data];
          
          } 
      );
    
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.supp_code == '' || this.supp_code == '0' ){
          this.showError('Invalid Code.')
          return
        }
    
        this.service.deletedatawithparam('custsupp',this.supp_code, this.supp_type)
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.supp_code = '';
              this.supp_name = '';
              this.supp_isActive = true
              this.supp_address = '';
              this.supp_contact = '';
            }
          } 
        );
      }
    
    }
    
    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) || 
                ( d.address.toLowerCase().indexOf(val) !== -1 || !val ) ||
                ( d.contact.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.table.offset = 0;
    }

    update(){

      if (this.supp_code == '' || this.supp_code == '0' ){
        this.showError('Code cannot be empty or Zero.')
        return
      }
  
      if (this.supp_name == '' ){
        this.showError('Name cannot be empty.')
        return
      }
      
      this.suppliers[0].code = this.supp_code;
      this.suppliers[0].name = this.supp_name;
      this.suppliers[0].isactive = this.supp_isActive;
      this.suppliers[0].address = this.supp_address;
      this.suppliers[0].contact = this.supp_contact;

      this.service.updateDatawithparam('custsupp',this.supp_code, this.supp_type, this.suppliers )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess('Record updated.'); 
            this.getdata();
          }  
        } 
      );
      this.selected = [];
      this.list = true
      this.entry = false
    }

    refresh(){
      
      this.list = false
      this.entry = true
      this.saveshow = true

      this.getMax();
      // this.supp_code = this.max_id;
      this.supp_name = '';
      this.supp_isActive = true;
      this.supp_address = '';
      this.supp_contact = '';
      this.enabledisable(true)
      this.selected = [];
    }
    
    checkactive(){
      this.supp_isActive = !this.supp_isActive
    }

    onSelect({ selected }) {

      this.supp_code = this.selected[0].code;
      this.supp_name = this.selected[0].name;
      this.supp_isActive = this.selected[0].isactive;
      this.supp_address = this.selected[0].address;
      this.supp_contact = this.selected[0].contact;
      this.enabledisable(false)

    }
  
    enabledisable(action:boolean){
      this.newbutton = action;
      this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

    

}
