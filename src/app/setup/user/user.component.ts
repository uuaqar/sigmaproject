import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { error } from 'util';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { CommonModule } from '@angular/common';  
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html',

  providers : [ DatePipe ]

  })
export class UserComponent {

  usr_name:string = '';
  usr_id:string = '';
  usr_pwd:string = '';
  usr_isActive:boolean = false;
  usr_firstlogin: boolean = true;
  usr_address:string = '';
  usr_email:string = '';
  usr_periodstr:string = '';
  private usr_period:any;

  users = new Array();
  rows:any;
  result:any;
  selected = [];
  temp = [];
  
  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;
  enableID:boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager, private datePipe:DatePipe ) { 
        
      this.getdata();
      this.enabledisable(true);
      this.users = [{'id':'', 'name': '', 'isactive':true, 'address':''}]
      this.usr_period = new Date();
      this.usr_periodstr = this.datePipe.transform(this.usr_period, "mm/dd/yyyy")
         
    }
    
  columns = [
    { name: 'ID' },
    { name: 'Name' },
    { name: 'Address' },
  ];
  
  save(){


    if (this.usr_id == '' ){
        this.showError('ID cannot be empty.')
        return
    }

    if (this.usr_name == '' ){
      this.showError('Name cannot be empty.')
      return
    }
    
    // if (this.usr_pwd == '' ){
    //   this.showError('Password cannot be empty.')
    //   return
    // }

    this.users[0].id = this.usr_id;
    this.users[0].name = this.usr_name;
    // this.users[0].password = this.usr_pwd;
    this.users[0].isactive = this.usr_isActive;
    this.users[0].address = this.usr_address
    this.users[0].firstlogin = this.usr_firstlogin
    this.users[0].email = this.usr_email;
    this.users[0].period = this.usr_period;

     this.result = this.service.saveData('adduser',this.users)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.usr_id = '';
            this.usr_name = '';
            // this.usr_pwd = '';
            this.usr_isActive = true;
            this.usr_address = '';
          }
         
        },
        
      );

      this.list = true
      this.entry = false      
    }  

    getdata(){
      this.service.getData('user')
      .subscribe( 
        data => {
          this.rows = data
          // this.selected = [data[0]];
          // this.onSelect(data[0]);
          this.enabledisable(true)
          // this.refresh();
          this.temp = [...data];

          } 
      );
    
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.usr_id == '' ){
          this.showError('Invalid ID.')
          return
        }
    
        this.service.deletedata('user',this.usr_id.toString())
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.usr_id = '';
              this.usr_name = '';
              // this.usr_pwd = '';
              this.usr_isActive = true
              this.usr_address = '';
            }
          } 
        );
      }
    
    }

    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return ( d.id.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) || 
                ( d.address.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.table.offset = 0;
    }

    update(){

      if (this.usr_id == '' ){
        this.showError('ID cannot be empty.')
        return
      }
  
      if (this.usr_name == '' ){
        this.showError('Name cannot be empty.')
        return
      }
      
      // if (this.usr_pwd == '' ){
      //   this.showError('Password cannot be empty.')
      //   return
      // }

      this.users[0].id = this.usr_id;
      this.users[0].name = this.usr_name;
      // this.users[0].password = this.usr_pwd;
      this.users[0].isactive = this.usr_isActive;
      this.users[0].address = this.usr_address;
      this.users[0].firstlogin = this.usr_firstlogin;
      this.users[0].email = this.usr_email;
      this.users[0].period = this.usr_period;

      this.service.updateData('user',this.usr_id.toString(), this.users, )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess('Record updated.'); 
            this.getdata();
          }  
        } 
      );
      this.selected = [];
      this.list = true
      this.entry = false

    }

    refresh(){

      this.list = false
      this.entry = true
      this.saveshow = true

      this.usr_id = '';
      this.usr_name = '';
      // this.usr_pwd = '';
      this.usr_isActive = true;
      this.usr_address = '';
      this.usr_email = '';
      this.usr_firstlogin = true;
      // this.usr_period = new Date();
      this.enableID = true;
      this.selected = [];
    }
    
    onSelect({ selected }) {

      this.usr_id = this.selected[0].id;
      this.usr_name = this.selected[0].name;
      // this.usr_pwd = this.selected[0].password;
      this.usr_isActive = this.selected[0].isactive;
      this.usr_address = this.selected[0].address;
      this.usr_firstlogin = this.selected[0].firstlogin;
      this.usr_email = this.selected[0].email;
      this.users[0].period = this.usr_period;
      this.enabledisable(false)

    }

    enabledisable(action:boolean){
      // this.newbutton = action;
      // this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
      this.enableID = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

    

}
