import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { error } from 'util';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
})
export class ProductComponent  {
  AllProducts: any;
  AllStore: any;
  AllUnits: any;
  AlliAtt1: any;
  AlliAtt2: any;
  AlliAtt3: any;

  iAtt1_desc: string = '';
  iAtt1Data = [];
  iAtt1_rows: any;
  iAtt1Select: boolean;
  open_iAtt1: boolean = true;
  find_iAtt1: any;
  iAtt1_selected = [];

  iAtt2_desc: string = '';
  iAtt2Data = [];
  iAtt2_rows: any;
  iAtt2Select: boolean;
  open_iAtt2: boolean = true;
  find_iAtt2: any;
  iAtt2_selected = [];

  iAtt3_desc: string = '';
  iAtt3Data = [];
  iAtt3_rows: any;
  iAtt3Select: boolean;
  open_iAtt3: boolean = true;
  find_iAtt3: any;
  iAtt3_selected = [];


  pUnit_desc: string = '';
  sUnit_desc: string = '';
  SUnitData = [];
  PUnitData = [];
  Punit_rows: any;
  Sunit_rows: any;
  pUnitSelect: boolean;
  sUnitSelect: boolean;
  open_pUnit: boolean = true;
  open_sUnit: boolean = true;
  product_att1: string = '';
  product_att2: string = '';
  product_att3: string = '';
  product_sUnit: string = '';
  product_pUnit: string = '';

  storeselect: boolean;
  open: boolean = true;

  product_name:string = '';
  product_code:string = '';
  product_isActive:boolean = false;
  product_store:string = '';
  store_desc:string = '';
  max_id:string = '0';
  products = new Array();
  rows:any;
  store_rows:any;
  find_rows:any;
  find_punit:any;
  find_sunit: any;
  row:any;  
  MaxNo:any;
  result:any;
  selected = [];
  store_selected = [];
  pUnit_selected = [];
  sUnit_selected = [];

  prodData = [];
  storeData = [];
  public primaryModal;

  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;

  @ViewChild(DatatableComponent) primaryTable: DatatableComponent;
  @ViewChild(DatatableComponent) storetable: DatatableComponent;
  @ViewChild(DatatableComponent) pUnitTable: DatatableComponent;
  @ViewChild(DatatableComponent) sUnitTable: DatatableComponent;
  @ViewChild(DatatableComponent) iAtt1Table: DatatableComponent;
  @ViewChild(DatatableComponent) iAtt2Table: DatatableComponent;
  @ViewChild(DatatableComponent) iAtt3Table: DatatableComponent;

  @ViewChild('primaryModal') storeModal: any;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager) { 
        
      this.getdata();
      this.getMax();
      this.getStoreData();
      this.getUnitData();

      this.getiAtt1Data();
      this.getiAtt2Data();
      this.getiAtt3Data();
      

      this.enabledisable(true);
      this.products = [{'code':'', 'name': '', 'isactive':true, 'store': '',
                        'purchase_unit': '', 'item_attribute1': '',
                        'item_attribute2': '', 'item_attribute3': '',
                        'storing_unit': '' }]
    }
    
  columns = [
    { name: 'Code', width: 50 },
    { name: 'Name' },
    { name: 'Active'},
    { name: 'Store'}
  ];

  getMax(){
    this.service.getMax('product')
    .subscribe( 
      data => {
        this.MaxNo = data
        if (data.length === 0 ){
          this.max_id = '1'
        } else {
          this.max_id = String(Number(this.MaxNo[0]._id) + 1);
        }
        this.product_code = this.max_id;
        } 
    );
  }

  store_columns = [
    { id: 'store_code', name: 'Code', width: 50 },
    { id: 'store_name', name: 'Name' },
    { id: 'store_isActive', name: 'Active'},
  ];

  unit_columns = [
    { id: 'unit_code', name: 'Code', width: 50 },
    { id: 'unit_name', name: 'Name' },
    { id: 'unit_isActive', name: 'Active'},
  ];

  save(){
    
    if (this.product_code == '' ){
        this.showError('Code cannot be empty.')
        return
    }

    if (this.product_name == '' ){
      this.showError('Name cannot be empty.')
      return
    }
    
    this.products[0].code = this.max_id;
    this.products[0].name = this.product_name;
    this.products[0].isactive = this.product_isActive;
    this.products[0].store = this.product_store;

    this.products[0].purchase_unit = this.product_pUnit;
    this.products[0].item_attribute1 = this.product_att1;
    this.products[0].item_attribute2 = this.product_att2;
    this.products[0].item_attribute3 = this.product_att3;
    this.products[0].storing_unit = this.product_sUnit;
   
     this.result = this.service.saveData('addproduct',this.products)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.product_code = '';
            this.product_name = '';
            this.product_isActive = true;
            this.product_store = '';
            this.store_desc = '';
            this.store_selected = []
          }
        },
      );

      this.list = true
      this.entry = false
      
      this.closestore()
    }  

    getdata(){
      this.service.getData('product')
      .subscribe( 
        prodData => {
          this.rows = prodData
          this.AllProducts = prodData
          
          this.enabledisable(true)
          // this.refresh();

          this.prodData = [...prodData];
          } 
      );
    
    }

    getStoreData(){
      this.service.getData('store')
      .subscribe( 
        data_store => {
          this.store_rows = data_store
          this.AllStore = data_store

          this.storeData = [...data_store];
        } 
      );
    }

    getUnitData(){
      this.service.getData('unit')
      .subscribe( 
        data_unit => {
          this.Punit_rows = data_unit
          this.Sunit_rows = data_unit
          this.AllUnits = data_unit

          this.PUnitData = [...data_unit];
          this.SUnitData = [...data_unit];
        } 
      );
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.product_code == '' ){
          this.showError('Invalid Code.')
          return
        }
    
        this.service.deletedata('product',this.product_code.toString())
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.product_code = '';
              this.product_name = '';
              this.product_isActive = true;
              this.product_store = '';
              this.store_desc = '';
              this.store_selected = []
            }
          } 
        );
      }
    
    }

    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
      this.getdata();
    }

    filterActive(event){
      let val = event.target.value.toLowerCase();;
      let filterActive:boolean
      this.prodData = [...this.AllProducts];

      if (val == 'all'){
        // filter our data
        const temp = this.prodData.filter(function(d) {
          return ( d.isactive == true ) ||
                ( d.isactive == false ) ;
        });

        // update the rows
        this.rows = temp;   
        this.prodData = [...temp]     
        
      } else {
        
        if (val == 'true'){
          filterActive = true
        }else if (val == 'false'){
          filterActive = false
        }

        // filter our data
        const temp = this.prodData.filter(function(d) {
          return ( d.isactive == filterActive )  ;
        });
    
        // update the rows
        this.rows = temp;
        this.prodData = [...temp]
      }
      
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.prodData.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.primaryTable.offset = 0;
    }

    filterStoreActive(event){
      let val = event.target.value.toLowerCase();;
      let filterActive:boolean
      this.storeData = [...this.AllStore];

      if (val == 'all'){
        // filter our data
        const temp = this.storeData.filter(function(d) {
          return ( d.isactive == true ) ||
                ( d.isactive == false ) ;
        });

        // update the rows
        this.store_rows = temp;    
        this.storeData = [...temp]    
        
      } else {
        
        if (val == 'true'){
          filterActive = true
        }else if (val == 'false'){
          filterActive = false
        }

        // filter our data
        const temp = this.storeData.filter(function(d) {
          return ( d.isactive == filterActive )  ;
        });
    
        // update the rows
        this.store_rows = temp;
        this.storeData = [...temp]    
      }
      
    }

    updateStoreFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.storeData.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.store_rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.storetable.offset = 0;
    }

    filterPUnitActive(event){
      let val = event.target.value.toLowerCase();;
      let filterActive:boolean
      this.PUnitData = [...this.AllUnits];

      if (val == 'all'){
        // filter our data
        const temp = this.PUnitData.filter(function(d) {
          return ( d.isactive == true ) ||
                ( d.isactive == false ) ;
        });

        // update the rows
        this.Punit_rows = temp;    
        this.PUnitData = [...temp]    
        
      } else {
        
        if (val == 'true'){
          filterActive = true
        }else if (val == 'false'){
          filterActive = false
        }

        // filter our data
        const temp = this.PUnitData.filter(function(d) {
          return ( d.isactive == filterActive )  ;
        });
    
        // update the rows
        this.Punit_rows = temp;
        this.PUnitData = [...temp]    
      }
      
    }

    updatePUnitFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.PUnitData.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.Punit_rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.pUnitTable.offset = 0;
    }

    filterSUnitActive(event){
      let val = event.target.value.toLowerCase();;
      let filterActive:boolean
      this.SUnitData = [...this.AllUnits];

      if (val == 'all'){
        // filter our data
        const temp = this.SUnitData.filter(function(d) {
          return ( d.isactive == true ) ||
                ( d.isactive == false ) ;
        });

        // update the rows
        this.Sunit_rows = temp;    
        this.SUnitData = [...temp]    
        
      } else {
        
        if (val == 'true'){
          filterActive = true
        }else if (val == 'false'){
          filterActive = false
        }

        // filter our data
        const temp = this.SUnitData.filter(function(d) {
          return ( d.isactive == filterActive )  ;
        });
    
        // update the rows
        this.Sunit_rows = temp;
        this.SUnitData = [...temp]    
      }
      
    }

    updateSUnitFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.SUnitData.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.Sunit_rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.sUnitTable.offset = 0;
    }

    update(){

      if (this.product_code == '' ){
        this.showError('Code cannot be empty.')
        return
      }
  
      if (this.product_name == '' ){
        this.showError('Name cannot be empty.')
        return
      }
      
      this.products[0].code = this.product_code;
      this.products[0].name = this.product_name;
      this.products[0].isactive = this.product_isActive;
      this.products[0].store = this.product_store;

      this.products[0].purchase_unit = this.product_pUnit;
      this.products[0].item_attribute1 = this.product_att1;
      this.products[0].item_attribute2 = this.product_att2;
      this.products[0].item_attribute3 = this.product_att3;
      this.products[0].storing_unit = this.product_sUnit;
   
      this.service.updateData('product',this.product_code.toString(), this.products, )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess(`Record updated.` ); 
            this.getdata();
          }  
        } 
      );
      this.selected = [];
      this.list = true
      this.entry = false
      
      this.closestore()
      this.closePUnit()
      this.closeSUnit()
    }

    refresh(){

      this.list = false
      this.entry = true
      this.saveshow = true

      this.getMax();
      // this.product_code = '';
      this.product_name = '';
      this.product_isActive = true;
      this.product_store = '';
      this.store_desc = '';

      this.pUnit_desc = '';
      this.sUnit_desc = '';
      this.product_pUnit = '';
      this.product_sUnit = '';

      // this.enabledisable(true)
      this.selected = [];
      this.store_selected = []
    }
    
    checkactive(){
      this.product_isActive = !this.product_isActive
    }

    getStoreDesc(product_store: string){

      if(product_store) {
        this.service.findData('store', product_store)
        .subscribe(
          data_find => {
            this.find_rows = data_find
            this.setStoreDesc()
          } 
        );
      }
    }

    getPUnitDesc(product_pUnit: string){

      if(product_pUnit) {
        this.service.findData('unit', product_pUnit)
        .subscribe(
          data_punit => {
            this.find_punit = data_punit
            this.setPUnitDesc()
          } 
        );
      } else {
        this.pUnit_desc = ''
      }
    }

    getSUnitDesc(product_sUnit: string){

      if(product_sUnit) {
        this.service.findData('unit', product_sUnit)
        .subscribe(
          data_sunit => {
            this.find_sunit = data_sunit
            this.setSUnitDesc()
          } 
        );
      } else {
        this.sUnit_desc = ''
      }
    }

    showstore(){
      this.storeselect = true
      this.open = false
    }

    closestore(){
      this.storeselect = false
      this.open = true
    }

    showPUnit(){
      this.pUnitSelect = true
      this.open_pUnit = false
    }

    closePUnit(){
      this.pUnitSelect = false
      this.open_pUnit = true
    }

    showSUnit(){
      this.sUnitSelect = true
      this.open_sUnit = false
    }

    closeSUnit(){
      this.sUnitSelect = false
      this.open_sUnit = true
    }

    setStoreDesc(){
      this.store_desc = this.find_rows[0].name;
    }

    setPUnitDesc(){
      this.pUnit_desc = this.find_punit[0].name;
    }

    setSUnitDesc(){
      this.sUnit_desc = this.find_sunit[0].name;
    }

    onSelect({ selected }) {

      this.product_code = this.selected[0].code;
      this.product_name = this.selected[0].name;
      this.product_isActive = this.selected[0].isactive;
      this.product_store = this.selected[0].store;

     this.product_pUnit = this.selected[0].purchase_unit
     this.product_att1 = this.selected[0].item_attribute1
     this.product_att2 = this.selected[0].item_attribute2
     this.product_att3 =  this.selected[0].item_attribute3
     this.product_sUnit =  this.selected[0].storing_unit
     
      this.getStoreDesc(this.product_store);
      this.getPUnitDesc(this.product_pUnit);
      this.getSUnitDesc(this.product_sUnit);

      this.getiAtt1Desc(this.product_att1);
      this.getiAtt2Desc(this.product_att2);
      this.getiAtt3Desc(this.product_att3);
      
      this.enabledisable(false)
    }

    onSelectStore({ selected }){
      
      this.product_store = this.store_selected[0].code;
      this.store_desc = this.store_selected[0].name;
      
      this.closestore()
    }

    onSelectPUnit({ selected }){
      
      this.product_pUnit = this.pUnit_selected[0].code;
      this.pUnit_desc = this.pUnit_selected[0].name;

      this.closePUnit();
      
    }

    onSelectSUnit({ selected }){
      
      this.product_sUnit = this.sUnit_selected[0].code;
      this.sUnit_desc = this.sUnit_selected[0].name;

      this.closeSUnit();
      
    }

    // Item Attribute 1

    iAtt1_columns = [
      { id: 'iAtt1_code', name: 'Code', width: 50 },
      { id: 'iAtt1_name', name: 'Name' },
      { id: 'iAtt1_isActive', name: 'Active'},
    ];

    getiAtt1Data(){
      this.service.getData('iattrbt1')
      .subscribe( 
        data_iAtt1 => {
          this.iAtt1_rows = data_iAtt1
          this.AlliAtt1 = data_iAtt1

          this.iAtt1Data = [...data_iAtt1];
        } 
      );
    }

    filteriAtt1Active(event){
      let val = event.target.value.toLowerCase();;
      let filterActive:boolean
      this.iAtt1Data = [...this.AlliAtt1];

      if (val == 'all'){
        // filter our data
        const temp = this.iAtt1Data.filter(function(d) {
          return ( d.isactive == true ) ||
                ( d.isactive == false ) ;
        });

        // update the rows
        this.iAtt1_rows = temp;    
        this.iAtt1Data = [...temp]    
        
      } else {
        
        if (val == 'true'){
          filterActive = true
        }else if (val == 'false'){
          filterActive = false
        }

        // filter our data
        const temp = this.iAtt1Data.filter(function(d) {
          return ( d.isactive == filterActive )  ;
        });
    
        // update the rows
        this.iAtt1_rows = temp;
        this.iAtt1Data = [...temp]    
      }
      
    }

    updateiAtt1Filter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.iAtt1Data.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.iAtt1_rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.iAtt1Table.offset = 0;
    }

    getiAtt1Desc(product_iAtt1: string){

      if(product_iAtt1) {
        this.service.findData('iattrbt1', product_iAtt1)
        .subscribe(
          data_iAtt1 => {
            this.find_iAtt1 = data_iAtt1
            this.setiAtt1Desc()
          } 
        );
      } else {
        this.iAtt1_desc = ''
      }
    }

    setiAtt1Desc(){
      this.iAtt1_desc = this.find_iAtt1[0].name;
    }

    showiAtt1(){
      this.iAtt1Select = true
      this.open_iAtt1 = false
    }

    closeiAtt1(){
      this.iAtt1Select = false
      this.open_iAtt1 = true
    }

    onSelectiAtt1({ selected }){
      
      this.product_att1 = this.iAtt1_selected[0].code;
      this.iAtt1_desc = this.iAtt1_selected[0].name;

      this.closeiAtt1();
      
    }
    
    // Item Attribute 2

    iAtt2_columns = [
      { id: 'iAtt2_code', name: 'Code', width: 50 },
      { id: 'iAtt2_name', name: 'Name' },
      { id: 'iAtt2_isActive', name: 'Active'},
    ];

    getiAtt2Data(){
      this.service.getData('iattrbt2')
      .subscribe( 
        data_iAtt2 => {
          this.iAtt2_rows = data_iAtt2
          this.AlliAtt2 =  data_iAtt2
          this.iAtt2Data = [...data_iAtt2];
        } 
      );
    }

    filteriAtt2Active(event){
      let val = event.target.value.toLowerCase();;
      let filterActive:boolean
      this.iAtt2Data = [...this.AlliAtt2];

      if (val == 'all'){
        // filter our data
        const temp = this.iAtt2Data.filter(function(d) {
          return ( d.isactive == true ) ||
                ( d.isactive == false ) ;
        });

        // update the rows
        this.iAtt2_rows = temp;    
        this.iAtt2Data = [...temp]    
        
      } else {
        
        if (val == 'true'){
          filterActive = true
        }else if (val == 'false'){
          filterActive = false
        }

        // filter our data
        const temp = this.iAtt2Data.filter(function(d) {
          return ( d.isactive == filterActive )  ;
        });
    
        // update the rows
        this.iAtt2_rows = temp;
        this.iAtt2Data = [...temp]    
      }
      
    }

    updateiAtt2Filter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.iAtt2Data.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.iAtt2_rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.iAtt2Table.offset = 0;
    }

    getiAtt2Desc(product_iAtt2: string){

      if(product_iAtt2) {
        this.service.findData('iattrbt2', product_iAtt2)
        .subscribe(
          data_iAtt2 => {
            this.find_iAtt2 = data_iAtt2
            this.setiAtt2Desc()
          } 
        );
      } else {
        this.iAtt2_desc = ''
      }
    }

    setiAtt2Desc(){
      this.iAtt2_desc = this.find_iAtt2[0].name;
    }

    showiAtt2(){
      this.iAtt2Select = true
      this.open_iAtt2 = false
    }

    closeiAtt2(){
      this.iAtt2Select = false
      this.open_iAtt2 = true
    }

    onSelectiAtt2({ selected }){
      
      this.product_att2 = this.iAtt2_selected[0].code;
      this.iAtt2_desc = this.iAtt2_selected[0].name;

      this.closeiAtt2();
      
    }

    // Item Attribute 3

    iAtt3_columns = [
      { id: 'iAtt3_code', name: 'Code', width: 50 },
      { id: 'iAtt3_name', name: 'Name' },
      { id: 'iAtt3_isActive', name: 'Active'},
    ];

    getiAtt3Data(){
      this.service.getData('iattrbt3')
      .subscribe( 
        data_iAtt3 => {
          this.iAtt3_rows = data_iAtt3
          this.AlliAtt3 = data_iAtt3

          this.iAtt3Data = [...data_iAtt3];
        } 
      );
    }

    filteriAtt3Active(event){
      let val = event.target.value.toLowerCase();;
      let filterActive:boolean
      this.iAtt3Data = [...this.AlliAtt3];

      if (val == 'all'){
        // filter our data
        const temp = this.iAtt3Data.filter(function(d) {
          return ( d.isactive == true ) ||
                ( d.isactive == false ) ;
        });

        // update the rows
        this.iAtt3_rows = temp;    
        this.iAtt3Data = [...temp]    
        
      } else {
        
        if (val == 'true'){
          filterActive = true
        }else if (val == 'false'){
          filterActive = false
        }

        // filter our data
        const temp = this.iAtt3Data.filter(function(d) {
          return ( d.isactive == filterActive )  ;
        });
    
        // update the rows
        this.iAtt3_rows = temp;
        this.iAtt3Data = [...temp]    
      }
      
    }

    updateiAtt3Filter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.iAtt3Data.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.iAtt3_rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.iAtt3Table.offset = 0;
    }

    getiAtt3Desc(product_iAtt3: string){

      if(product_iAtt3) {
        this.service.findData('iattrbt3', product_iAtt3)
        .subscribe(
          data_iAtt3 => {
            this.find_iAtt3 = data_iAtt3
            this.setiAtt3Desc()
          } 
        );
      } else {
        this.iAtt3_desc = ''
      }
    }

    setiAtt3Desc(){
      this.iAtt3_desc = this.find_iAtt3[0].name;
    }

    showiAtt3(){
      this.iAtt3Select = true
      this.open_iAtt3 = false
    }

    closeiAtt3(){
      this.iAtt3Select = false
      this.open_iAtt3 = true
    }

    onSelectiAtt3({ selected }){
      
      this.product_att3 = this.iAtt3_selected[0].code;
      this.iAtt3_desc = this.iAtt3_selected[0].name;

      this.closeiAtt3();
      
    }

    enabledisable(action:boolean){
      this.newbutton = action;
      this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

}
