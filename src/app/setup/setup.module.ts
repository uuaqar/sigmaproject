import { NgModule } from '@angular/core';
import { UserComponent} from './user/user.component'
import { SetupRoutingModule } from './setup-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RESTService } from '../shared/rest.service';
import { Globals } from '../vo/globals';
import { FormsModule } from '@angular/forms';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { StoreComponent } from './store/store.component';
import { ProductComponent } from './product/product.component';
import { CustomerComponent } from './customer/customer.component';
import { SupplierComponent } from './supplier/supplier.component';
import { ChartOfAccComponent } from './chartofacc/chartofacc.component';
// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';
import { CityComponent } from './city/city.component';
import { SalesareaComponent } from './salesarea/salesarea.component';
import { UnitComponent } from './unit/unit.component';
import { Partyattribute1Component } from './partyattribute1/partyattribute1.component';
import { Partyattribute2Component } from './partyattribute2/partyattribute2.component';
import { Partyattribute3Component } from './partyattribute3/partyattribute3.component';
import { Itemattribute1Component } from 'app/setup/itemattribute1/itemattribute1.component';
import { Itemattribute2Component } from 'app/setup/itemattribute2/itemattribute2.component';
import { Itemattribute3Component } from 'app/setup/itemattribute3/itemattribute3.component';

import { SalemanComponent } from './saleman/saleman.component';  
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    SetupRoutingModule,
    NgxDatatableModule,
    FormsModule,
    ToastModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    CommonModule,
   
  ],
  declarations: [
    UserComponent,
    StoreComponent,
    ProductComponent,
    CustomerComponent,
    SupplierComponent,
    ChartOfAccComponent,
    CityComponent,
    SalesareaComponent,
    UnitComponent,
    Partyattribute1Component,
    Partyattribute2Component,
    Partyattribute3Component,
    Itemattribute1Component,
    Itemattribute2Component,
    Itemattribute3Component,
    SalemanComponent,
  ],
  providers:[
    RESTService,
    Globals
  ]
})
export class SetupModule { }
