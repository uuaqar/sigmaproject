import { Component, OnInit, ViewContainerRef, ViewChild, } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { error } from 'util';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-partyattribute3',
  templateUrl: './partyattribute3.component.html',
  
  })
export class Partyattribute3Component {
  
  patrbt3_name:string = '';
  patrbt3_code:string = '';
  patrbt3_isActive:boolean = false;
  patrbt3_location:string = '';
  pattrbts3 = new Array();
  max_id: string = '0';
  MaxNo: any;
  rows:any;
  row:any;
  result:any;
  selected = [];
  temp = [];

  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager) { 
        
      this.getdata();
      // this.getMax();
      this.enabledisable(true);
      this.pattrbts3 = [{'code':'', 'name': '', 'isactive':true, 'province':''}]
    
    }
    
  columns = [
    { name: 'Code', width: 50 },
    { name: 'Name' },
    { name: 'Active'},
    // { name: 'Location' }
  ];

  getMax(){
    this.service.getMax('pattrbt3')
    .subscribe( 
      data => {
        this.MaxNo = data
        if (data.length === 0 ){
          this.max_id = '1'
        } else {
          this.max_id = String(Number(this.MaxNo[0]._id) + 1);
        }
        this.patrbt3_code = this.max_id;
        } 
    );
  }
  
  save(){
    
    if (this.patrbt3_code == '' ){
        this.showError('Code cannot be empty.')
        return
    }

    if (this.patrbt3_name == '' ){
      this.showError('Name cannot be empty.')
      return
    }
    
    this.pattrbts3[0].code = this.max_id;
    this.pattrbts3[0].name = this.patrbt3_name;
    this.pattrbts3[0].isactive = this.patrbt3_isActive;
    // this.pattrbts3[0].location = this.patrbt3_location
    
     this.result = this.service.saveData('addpattrbt3',this.pattrbts3)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.patrbt3_code = '';
            this.patrbt3_name = '';
            this.patrbt3_isActive = true;
            // this.patrbt3_location = '';
          }
         
        },
        
      );
      
      this.list = true
      this.entry = false
    }  

    getdata(){
      this.service.getData('pattrbt3')
      .subscribe( 
        data => {
          this.rows = data
          // this.selected = [data[0]];
          // this.onSelect(data[0]);
          
          this.enabledisable(true)
          // this.refresh();
          this.temp = [...data];
          } 
      );
    
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.patrbt3_code == '' ){
          this.showError('Invalid Code.')
          return
        }
    
        this.service.deletedata('pattrbt3',this.patrbt3_code.toString())
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.patrbt3_code = '';
              this.patrbt3_name = '';
              this.patrbt3_isActive = true
              // this.patrbt3_location = '';
            }
          } 
        );
      }
    
    }

    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.table.offset = 0;
    }

    update(){

      if (this.patrbt3_code == '' ){
        this.showError('Code cannot be empty.')
        return
      }
  
      if (this.patrbt3_name == '' ){
        this.showError('Name cannot be empty.')
        return
      }
      
      this.pattrbts3[0].code = this.patrbt3_code;
      this.pattrbts3[0].name = this.patrbt3_name;
      this.pattrbts3[0].isactive = this.patrbt3_isActive;
      // this.pattrbts3[0].location = this.patrbt3_location

      this.service.updateData('pattrbt3',this.patrbt3_code.toString(), this.pattrbts3, )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess('Record updated.'); 
            this.getdata();
          }  
        } 
      );
      this.selected = [];
      this.list = true
      this.entry = false

    }

    refresh(){

      this.list = false
      this.entry = true
      this.saveshow = true

      this.getMax();
      // this.patrbt3_code = '';
      this.patrbt3_name = '';
      this.patrbt3_isActive = true;
      // this.patrbt3_location = '';
      // this.enabledisable(true)
      this.selected = [];
    }
    
    onSelect({ selected }) {

      this.patrbt3_code = this.selected[0].code;
      this.patrbt3_name = this.selected[0].name;
      this.patrbt3_isActive = this.selected[0].isactive;
      // this.patrbt3_location = this.selected[0].location;
      this.enabledisable(false)

    }
  
    enabledisable(action:boolean){
      this.newbutton = action;
      this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

    

}
