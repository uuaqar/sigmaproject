import { Component, OnInit, ViewContainerRef, ViewChild, } from '@angular/core';
import { RESTService } from '../../shared/rest.service';
import { ActivatedRoute,Router } from '@angular/router';
import { error } from 'util';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-unit',
  templateUrl: 'unit.component.html',

  })
export class UnitComponent {
  
  unit_name:string = '';
  unit_code:string = '';
  unit_isActive:boolean = false;
  unit_location:string = '';
  units = new Array();
  max_id: string = '0';
  MaxNo: any;
  rows:any;
  row:any;
  result:any;
  selected = [];
  temp = [];

  newbutton:boolean = true;
  savebutton:boolean = true;
  updatebutton:boolean = true;
  deletebutton:boolean = true;
  list:boolean = true;
  entry:boolean = false;
  saveshow:boolean = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private service:RESTService, private route: ActivatedRoute, private router: Router,
          public toastr: ToastsManager) { 
        
      this.getdata();
      // this.getMax();
      this.enabledisable(true);
      this.units = [{'code':'', 'name': '', 'isactive':true, 'province':''}]
    
    }
    
  columns = [
    { name: 'Code', width: 50 },
    { name: 'Name' },
    { name: 'Active'},
    // { name: 'Location' }
  ];

  getMax(){
    this.service.getMax('unit')
    .subscribe( 
      data => {
        this.MaxNo = data
        if (data.length === 0 ){
          this.max_id = '1'
        } else {
          this.max_id = String(Number(this.MaxNo[0]._id) + 1);
        }
        this.unit_code = this.max_id;
        } 
    );
  }
  
  save(){
    
    if (this.unit_code == '' ){
        this.showError('Code cannot be empty.')
        return
    }

    if (this.unit_name == '' ){
      this.showError('Name cannot be empty.')
      return
    }
    
    this.units[0].code = this.max_id;
    this.units[0].name = this.unit_name;
    this.units[0].isactive = this.unit_isActive;
    // this.units[0].location = this.unit_location
    
     this.result = this.service.saveData('addunit',this.units)
      .subscribe(
        data1 => {
          
          if (data1.errmsg != '' && data1.errmsg != undefined ) {
            alert(data1.errmsg)
          }else
          {
            this.showSuccess('Record saved.'); 
            // alert('Data Updated');
            this.getdata();
            this.unit_code = '';
            this.unit_name = '';
            this.unit_isActive = true;
            // this.unit_location = '';
          }
         
        },
        
      );
      
      this.list = true
      this.entry = false
    }  

    getdata(){
      this.service.getData('unit')
      .subscribe( 
        data => {
          this.rows = data
          // this.selected = [data[0]];
          // this.onSelect(data[0]);
          
          this.enabledisable(true)
          // this.refresh();
          this.temp = [...data];
          } 
      );
    
    }

    delete(){
  
      if (confirm('Are you sure you want to delete?')) {

        if (this.unit_code == '' ){
          this.showError('Invalid Code.')
          return
        }
    
        this.service.deletedata('unit',this.unit_code.toString())
          .subscribe( 
          datadel => {
  
            if (datadel.errmsg != '' && datadel.errmsg != undefined ) {
              alert(datadel.errmsg)
            }else
            {
              this.showSuccess('Record deleted.'); 
              this.getdata();
              // this.refresh();
              this.selected = []
              this.unit_code = '';
              this.unit_name = '';
              this.unit_isActive = true
              // this.unit_location = '';
            }
          } 
        );
      }
    
    }

    edit(){
      this.list = false
      this.entry = true
      this.saveshow = false
    }

    cancel(){
      this.list = true
      this.entry = false
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return ( d.code.toLowerCase().indexOf(val) !== -1 || !val )  || 
                ( d.name.toLowerCase().indexOf(val) !== -1 || !val ) ;
      });
  
      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.table.offset = 0;
    }

    update(){

      if (this.unit_code == '' ){
        this.showError('Code cannot be empty.')
        return
      }
  
      if (this.unit_name == '' ){
        this.showError('Name cannot be empty.')
        return
      }
      
      this.units[0].code = this.unit_code;
      this.units[0].name = this.unit_name;
      this.units[0].isactive = this.unit_isActive;
      // this.units[0].location = this.unit_location

      this.service.updateData('unit',this.unit_code.toString(), this.units, )
      .subscribe( 
        dataupdate => {
          if (dataupdate.errmsg != '' && dataupdate.errmsg != undefined ) {
            alert(dataupdate.errmsg)
          }else
          {
            this.showSuccess('Record updated.'); 
            this.getdata();
          }  
        } 
      );
      this.selected = [];
      this.list = true
      this.entry = false

    }

    refresh(){

      this.list = false
      this.entry = true
      this.saveshow = true

      this.getMax();
      // this.unit_code = '';
      this.unit_name = '';
      this.unit_isActive = true;
      // this.unit_location = '';
      // this.enabledisable(true)
      this.selected = [];
    }
    
    onSelect({ selected }) {

      this.unit_code = this.selected[0].code;
      this.unit_name = this.selected[0].name;
      this.unit_isActive = this.selected[0].isactive;
      // this.unit_location = this.selected[0].location;
      this.enabledisable(false)

    }
  
    enabledisable(action:boolean){
      this.newbutton = action;
      this.savebutton = !action;
      this.updatebutton = action;
      this.deletebutton = action;
    }

    onActivate(event) {
      // console.log('Activate Event', event);
    }

    showError(msg:string) {
      this.toastr.error(msg, 'Error!');
    }

    showSuccess(msg:string) {
      this.toastr.success(msg, 'Successful');
    }

    

}
