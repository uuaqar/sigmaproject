import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component'
import { StoreComponent } from './store/store.component'
import { ProductComponent } from 'app/setup/product/product.component';
import { CustomerComponent } from 'app/setup/customer/customer.component';
import { SupplierComponent } from 'app/setup/supplier/supplier.component';
import { ChartOfAccComponent } from 'app/setup/chartofacc/chartofacc.component';
import { CityComponent } from 'app/setup/city/city.component';
import { SalesareaComponent } from 'app/setup/salesarea/salesarea.component';
import { UnitComponent } from 'app/setup/unit/unit.component';
import { Partyattribute1Component } from './partyattribute1/partyattribute1.component';
import { Partyattribute2Component } from './partyattribute2/partyattribute2.component';
import { Partyattribute3Component } from './partyattribute3/partyattribute3.component';  
import { SalemanComponent } from 'app/setup/saleman/saleman.component';
import { Itemattribute1Component } from 'app/setup/itemattribute1/itemattribute1.component';
import { Itemattribute2Component } from 'app/setup/itemattribute2/itemattribute2.component';
import { Itemattribute3Component } from 'app/setup/itemattribute3/itemattribute3.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'user/user',
        component: UserComponent,
      },
      {
        path: 'store/store',
        component: StoreComponent,
      },
      {
        path: 'product/product',
        component: ProductComponent,
      },
      {
        path: 'customer/customer',
        component: CustomerComponent,
      },
      {
        path: 'supplier/supplier',
        component: SupplierComponent,
      },
      {
        path: 'chartofacc/chartofacc',
        component: ChartOfAccComponent,
      },
      {
        path: 'city/city',
        component: CityComponent,
      },
      {
        path: 'salesarea/salesarea',
        component: SalesareaComponent,
      },
      {
        path: 'saleman/saleman',
        component: SalemanComponent,
      },
      {
        path: 'unit/unit',
        component: UnitComponent,
      },

      {
        path: 'partyattribute1/partyattribute1',
        component: Partyattribute1Component,
      },
      {
        path: 'partyattribute2/partyattribute2',
        component: Partyattribute2Component,
      },
      {
        path: 'partyattribute3/partyattribute3',
        component: Partyattribute3Component,
      },

      {
        path: 'itemattribute1/itemattribute1',
        component: Itemattribute1Component,
      },
      {
        path: 'itemattribute2/itemattribute2',
        component: Itemattribute2Component,
      },
      {
        path: 'itemattribute3/itemattribute3',
        component: Itemattribute3Component,
      },


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule { }
