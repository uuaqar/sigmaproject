import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};
  userName:string = '';


  constructor(
    private route: ActivatedRoute,
    private router: Router){

  }

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  onLogout()
  {
    localStorage.setItem('loggedin','0');
    this.router.navigateByUrl('auth/login');
  }

  ngOnInit(): void {
    if(localStorage.getItem('loggedin') == '1')
    {
      this.userName = localStorage.getItem('usrname');
    }
    else{
      this.router.navigateByUrl('auth/login');
    }
  }
}
