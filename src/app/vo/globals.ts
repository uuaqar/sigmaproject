export class Globals {
  hostname: string = window.location.hostname;
  port: string = window.location.port;
  weburl: string = 'http://' + this.hostname  + ':8080/';
  // weburl: string = 'http://' + this.hostname  + ':'+ this.port + '/';

  fn_parseFloatIgnoreCommas(varString:String) :any {
    let stringWithoutComma = varString.replace(',', '');
    return parseFloat(stringWithoutComma);
  }
}
