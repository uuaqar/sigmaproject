var Pattrbt2 = require("../partyattribute2/partyattribute2.model.js");

exports.create = function(req, res) {
    
    var pattrbt2 = new Pattrbt2 ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name });

    pattrbt2.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Pattrbt2.find(function(err,pattrbt2){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt2)
        }
    });
};

exports.findOne = function(req, res) {
    Pattrbt2.find({"_id": req.params.pattrbt2Code}, function(err,pattrbt2){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt2)
        }
    })
}

exports.getMax = function(req, res) {
    Pattrbt2.find().sort({"_id":-1}).limit(1).exec(function(err,pattrbt2){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt2)
        }
    })
};

exports.update = function(req, res) {

    Pattrbt2.update ({"_id": req.params.pattrbt2Code}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Pattrbt2.remove({"_id": req.params.pattrbt2Code}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}