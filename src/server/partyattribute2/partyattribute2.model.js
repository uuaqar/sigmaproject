var mongoose = require("../../../node_modules/mongoose");

    var Pattrbt2Schema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String
})

module.exports = mongoose.model('Pattrbt2', Pattrbt2Schema)