module.exports = function(app) {
    var pattrbt2 = require("../partyattribute2/partyattribute2.cotroller.js");
    
    app.post('/addpattrbt2', pattrbt2.create);
    app.get('/pattrbt2/:pattrbt2Code', pattrbt2.findOne);
    app.get('/pattrbt2', pattrbt2.findAll);
    app.get('/maxpattrbt2', pattrbt2.getMax);
    app.put('/pattrbt2/:pattrbt2Code', pattrbt2.update);
    app.delete('/pattrbt2/:pattrbt2Code', pattrbt2.delete);
    }
    