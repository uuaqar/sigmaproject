var mongoose = require("../../../node_modules/mongoose");

    var SalesAreaSchema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    type: String,
    name: String,
    parent: String,
})

module.exports = mongoose.model('SalesArea', SalesAreaSchema)