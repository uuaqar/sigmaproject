var SalesArea = require("../salesarea/salesarea.model.js");

exports.create = function(req, res) {
    
    var salesarea = new SalesArea ({code: req.body.code, _id:req.body.code, type:req.body.type, 
                            name: req.body.name, parent: req.body.parent });

    salesarea.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    SalesArea.find(function(err,salesarea){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(salesarea)
        }
    });
};

exports.getMax = function(req, res) {
    SalesArea.find().sort({"_id":-1}).limit(1).exec(function(err,salesarea){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(salesarea)
        }
    })
};

exports.getParent = function(req, res) {
    SalesArea.find().where({"type":'1'}).exec(function(err,salesarea){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(salesarea)
        }
    })
};

exports.findOne = function(req, res) {
    SalesArea.find({"_id": req.params.salesareaCode}, function(err,salesarea){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(salesarea)
        }
    })
}

exports.update = function(req, res) {

    SalesArea.update ({"_id": req.params.salesareaCode}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    SalesArea.remove({"_id": req.params.salesareaCode}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}