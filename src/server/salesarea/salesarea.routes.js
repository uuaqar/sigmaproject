module.exports = function(app) {
    var salesarea = require("../salesarea/salesarea.cotroller.js");
    
    app.post('/addsalesarea', salesarea.create);
    app.get('/salesarea', salesarea.findAll);
    app.get('/salesarea/:salesareaCode', salesarea.findOne);
    app.get('/maxsalesarea', salesarea.getMax);
    app.get('/parentsalesarea', salesarea.getParent);
    app.put('/salesarea/:salesareaCode', salesarea.update);
    app.delete('/salesarea/:salesareaCode', salesarea.delete);
    }
    