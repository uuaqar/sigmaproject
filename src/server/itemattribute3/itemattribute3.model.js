var mongoose = require("../../../node_modules/mongoose");

    var Iattrbt3Schema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String
})

module.exports = mongoose.model('Iattrbt3', Iattrbt3Schema)