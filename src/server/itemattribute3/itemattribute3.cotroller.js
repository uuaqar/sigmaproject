var Iattrbt3 = require("../itemattribute3/itemattribute3.model.js");

exports.create = function(req, res) {
    
    var iattrbt3 = new Iattrbt3 ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name });

    iattrbt3.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Iattrbt3.find(function(err,iattrbt3){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt3)
        }
    });
};

exports.findOne = function(req, res) {
    Iattrbt3.find({"_id": req.params.iattrbt3Code}, function(err,iattrbt3){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt3)
        }
    })
}

exports.getMax = function(req, res) {
    Iattrbt3.find().sort({"_id":-1}).limit(1).exec(function(err,iattrbt3){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt3)
        }
    })
};

exports.update = function(req, res) {

    Iattrbt3.update ({"_id": req.params.iattrbt3Code}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Iattrbt3.remove({"_id": req.params.iattrbt3Code}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}