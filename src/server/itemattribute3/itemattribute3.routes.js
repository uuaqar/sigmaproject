module.exports = function(app) {
    var iattrbt3 = require("../itemattribute3/itemattribute3.cotroller.js");
    
    app.post('/addiattrbt3', iattrbt3.create);
    app.get('/iattrbt3/:iattrbt3Code', iattrbt3.findOne);
    app.get('/iattrbt3', iattrbt3.findAll);
    app.get('/maxiattrbt3', iattrbt3.getMax);
    app.put('/iattrbt3/:iattrbt3Code', iattrbt3.update);
    app.delete('/iattrbt3/:iattrbt3Code', iattrbt3.delete);
    }
    