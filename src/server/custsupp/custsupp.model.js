
var mongoose = require("../../../node_modules/mongoose");
 
    var CustSuppSchema = mongoose.Schema({
    code: String,
    name: String,
    isactive: Boolean,
    type: String,
    address: String,
    both: String,
    city: String,
    email: String,
    cell1: String,
    cell2: String,
    cell3: String,
    sms_cell: String,
    longitude: Number,
    latitude: Number,
    saleman: String,
    store: String,
    sale_area: String,
    party_att1: String,
    party_att2: String,
    party_att3: String,
    gl_acc: String
    
})

CustSuppSchema.index({code: 1, type: 1}, {unique: true});

module.exports = mongoose.model('CustSupp', CustSuppSchema)


