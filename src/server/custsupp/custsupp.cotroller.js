var CustSupp = require("../custsupp/custsupp.model.js");

exports.create = function(req, res) {
    
    var custsupp = new CustSupp ({code:req.body.code, isactive:req.body.isactive, 
                            name:req.body.name, address:req.body.address, type:req.body.type,
                            both:req.body.both, city:req.body.city,
                            email:req.body.email,
                            cell1:req.body.cell1,
                            cell2:req.body.cell2,
                            cell3:req.body.cell3,
                            sms_cell:req.body.sms_cell,
                            longitude:req.body.longitude,
                            latitude:req.body.latitude,
                            saleman:req.body.saleman,
                            store:req.body.store,
                            sale_area:req.body.sale_area,
                            party_att1:req.body.party_att1,
                            party_att2:req.body.party_att2,
                            party_att3:req.body.party_att3,
                            gl_acc:req.body.gl_acc });

    custsupp.save(function(err, data){
        // console.log(data);
        if(err){
            console.log(err);
            res.send(err.errmsg);
        } else {
            console.log(data);
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    CustSupp.find({"type": req.params.type }, function(err,custsupp){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(custsupp)
        }
    });
};

exports.getMax = function(req, res) {
    CustSupp.find({"type": req.params.type }).sort({"_id":-1}).limit(1).exec(function(err,custsupp){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(custsupp)
        }
    })
};

exports.findOne = function(req, res) {
    CustSupp.find({"code": req.params.custsuppCode,"type": req.params.type }, function(err,custsupp){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(custsupp)
        }
    })
}

exports.update = function(req, res) {

    CustSupp.update ({"code": req.params.custsuppCode, "type": req.params.type}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    CustSupp.remove({"code": req.params.custsuppCode,"type": req.params.type}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}