module.exports = function(app) {
    var custsupp = require("../custsupp/custsupp.cotroller.js");
    
    app.post('/addcustsupp', custsupp.create);
    app.get('/custsupp/:type', custsupp.findAll);
    app.get('/custsupp/:custsuppCode/:type', custsupp.findOne);
    app.get('/maxcustsupp/:type', custsupp.getMax);
    app.put('/custsupp/:custsuppCode/:type', custsupp.update);
    app.delete('/custsupp/:custsuppCode/:type', custsupp.delete);
    
    }
    