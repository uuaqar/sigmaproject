module.exports = function(app) {
    var pattrbt1 = require("../partyattribute1/partyattribute1.cotroller.js");
    
    app.post('/addpattrbt1', pattrbt1.create);
    app.get('/pattrbt1/:pattrbt1Code', pattrbt1.findOne);
    app.get('/pattrbt1', pattrbt1.findAll);
    app.get('/maxpattrbt1', pattrbt1.getMax);
    app.put('/pattrbt1/:pattrbt1Code', pattrbt1.update);
    app.delete('/pattrbt1/:pattrbt1Code', pattrbt1.delete);
    }
    