var Pattrbt1 = require("../partyattribute1/partyattribute1.model.js");

exports.create = function(req, res) {
    
    var pattrbt1 = new Pattrbt1 ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name });

    pattrbt1.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Pattrbt1.find(function(err,pattrbt1){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt1)
        }
    });
};

exports.findOne = function(req, res) {
    Pattrbt1.find({"_id": req.params.pattrbt1Code}, function(err,pattrbt1){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt1)
        }
    })
}

exports.getMax = function(req, res) {
    Pattrbt1.find().sort({"_id":-1}).limit(1).exec(function(err,pattrbt1){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt1)
        }
    })
};

exports.update = function(req, res) {

    Pattrbt1.update ({"_id": req.params.pattrbt1Code}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Pattrbt1.remove({"_id": req.params.pattrbt1Code}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}