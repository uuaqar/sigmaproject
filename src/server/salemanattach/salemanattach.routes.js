module.exports = function(app) {
    var salemanattach = require("../salemanattach/salemanattach.cotroller.js");
    
    app.post('/addsalemanAttach', salemanattach.create);
    app.get('/salemanAttach/:salemanCode', salemanattach.findAll);
    app.get('/salemanAttach/:salemanCode/:seqno', salemanattach.findOne);
    app.get('/maxsalemanAttach/:salemanCode', salemanattach.getMax);
    app.put('/salemanAttach/:salemanCode/:seqno', salemanattach.update);
    app.delete('/salemanAttach/:salemanCode/:seqno', salemanattach.delete);
   

    }
    