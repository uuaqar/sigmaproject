
var mongoose = require("../../../node_modules/mongoose");
 
    var SalemanAttachSchema = mongoose.Schema({
    salmancode: String,
    seqno: String,
    _id: { type: "string" },
    name: String,
    file: {data:Buffer, contentType:String},
    ext: String
})

module.exports = mongoose.model('SalemanAttach', SalemanAttachSchema)


