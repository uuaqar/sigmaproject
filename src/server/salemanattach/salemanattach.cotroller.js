var SalemanAttach = require("../salemanattach/salemanattach.model.js");

exports.create = function(req, res) {
    
    var salemanAttach = new SalemanAttach ({salmancode:req.body.salmancode, seqno:req.body.seqno, _id:req.body.seqno, 
                                name:req.body.name, file:req.body.file, ext:req.body.ext
                           });

    salemanAttach.save(function(err, data){
        // console.log(data);
        if(err){
            console.log(err);
            res.send(err.errmsg);
        } else {
            console.log(data);
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    SalemanAttach.find({"salmancode": req.params.salemanCode }, function(err,salemanAttach){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(salemanAttach)
        }
    });
};

exports.getMax = function(req, res) {
    SalemanAttach.find({"salmancode": req.params.salemanCode }).sort({"_id":-1}).limit(1).exec(function(err,salemanAttach){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(salemanAttach)
        }
    })
};

exports.findOne = function(req, res) {
    SalemanAttach.find({"salmancode": req.params.salemanCode , "_id": req.params.seqno }, function(err,salemanAttach){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(salemanAttach)
        }
    })
}

exports.update = function(req, res) {

    SalemanAttach.update ({"salmancode": req.params.salemanCode , "_id": req.params.seqno }, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    SalemanAttach.remove({"salmancode": req.params.salemanCode , "_id": req.params.seqno }, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}
