var mongoose = require("../../../node_modules/mongoose");

    var Pattrbt3Schema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String
})

module.exports = mongoose.model('Pattrbt3', Pattrbt3Schema)