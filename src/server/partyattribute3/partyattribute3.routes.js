module.exports = function(app) {
    var pattrbt3 = require("../partyattribute3/partyattribute3.cotroller.js");
    
    app.post('/addpattrbt3', pattrbt3.create);
    app.get('/pattrbt3/:pattrbt3Code', pattrbt3.findOne);
    app.get('/pattrbt3', pattrbt3.findAll);
    app.get('/maxpattrbt3', pattrbt3.getMax);
    app.put('/pattrbt3/:pattrbt3Code', pattrbt3.update);
    app.delete('/pattrbt3/:pattrbt3Code', pattrbt3.delete);
    }
    