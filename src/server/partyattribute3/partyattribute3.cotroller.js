var Pattrbt3 = require("../partyattribute3/partyattribute3.model.js");

exports.create = function(req, res) {
    
    var pattrbt3 = new Pattrbt3 ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name });

    pattrbt3.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Pattrbt3.find(function(err,pattrbt3){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt3)
        }
    });
};

exports.findOne = function(req, res) {
    Pattrbt3.find({"_id": req.params.pattrbt3Code}, function(err,pattrbt3){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt3)
        }
    })
}

exports.getMax = function(req, res) {
    Pattrbt3.find().sort({"_id":-1}).limit(1).exec(function(err,pattrbt3){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(pattrbt3)
        }
    })
};

exports.update = function(req, res) {

    Pattrbt3.update ({"_id": req.params.pattrbt3Code}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Pattrbt3.remove({"_id": req.params.pattrbt3Code}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}