var Product = require("../product/product.model.js");

exports.create = function(req, res) {
    
    var product = new Product ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name, store: req.body.store ,
                            purchase_unit: req.body.purchase_unit,
                            item_attribute1: req.body.item_attribute3,
                            item_attribute2: req.body.item_attribute3,
                            item_attribute3: req.body.item_attribute3,
                            storing_unit: req.body.storing_unit
                        });
    product.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Product.find(function(err,product){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(product)
        }
    });
};

exports.getMax = function(req, res) {
    Product.find().sort({"_id":-1}).limit(1).exec(function(err,product){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(product)
        }
    })
};

exports.findOne = function(req, res) {
    Product.find({"_id": req.params.productCode}, function(err,product){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(product)
        }
    })
}

exports.update = function(req, res) {

    Product.update ({"_id": req.params.productCode}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Product.remove({"_id": req.params.productCode}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}