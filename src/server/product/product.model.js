var mongoose = require("../../../node_modules/mongoose");

    var ProductSchema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String,
    store: String,
    purchase_unit: String,
    item_attribute1: String,
    item_attribute2: String,
    item_attribute3: String,
    storing_unit: String
})

module.exports = mongoose.model('Product', ProductSchema)