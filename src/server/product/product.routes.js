module.exports = function(app) {
    var product = require("../product/product.cotroller.js");
    
    app.post('/addproduct', product.create);
    app.get('/product', product.findAll);
    app.get('/product/:productCode', product.findOne);
    app.get('/maxproduct', product.getMax);
    app.put('/product/:productCode', product.update);
    app.delete('/product/:productCode', product.delete);
    }
    