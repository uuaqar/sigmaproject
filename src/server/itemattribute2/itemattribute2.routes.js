module.exports = function(app) {
    var iattrbt2 = require("../itemattribute2/itemattribute2.cotroller.js");
    
    app.post('/addiattrbt2', iattrbt2.create);
    app.get('/iattrbt2/:iattrbt2Code', iattrbt2.findOne);
    app.get('/iattrbt2', iattrbt2.findAll);
    app.get('/maxiattrbt2', iattrbt2.getMax);
    app.put('/iattrbt2/:iattrbt2Code', iattrbt2.update);
    app.delete('/iattrbt2/:iattrbt2Code', iattrbt2.delete);
    }
    