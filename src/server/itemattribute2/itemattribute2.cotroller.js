var Iattrbt2 = require("../itemattribute2/itemattribute2.model.js");

exports.create = function(req, res) {
    
    var iattrbt2 = new Iattrbt2 ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name });

    iattrbt2.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Iattrbt2.find(function(err,iattrbt2){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt2)
        }
    });
};

exports.findOne = function(req, res) {
    Iattrbt2.find({"_id": req.params.iattrbt2Code}, function(err,iattrbt2){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt2)
        }
    })
}

exports.getMax = function(req, res) {
    Iattrbt2.find().sort({"_id":-1}).limit(1).exec(function(err,iattrbt2){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt2)
        }
    })
};

exports.update = function(req, res) {

    Iattrbt2.update ({"_id": req.params.iattrbt2Code}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Iattrbt2.remove({"_id": req.params.iattrbt2Code}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}