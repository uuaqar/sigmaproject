var mongoose = require("../../../node_modules/mongoose");

    var Iattrbt2Schema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String
})

module.exports = mongoose.model('Iattrbt2', Iattrbt2Schema)