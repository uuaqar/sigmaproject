var mongoose = require("../../../node_modules/mongoose");

    var UserSchema = mongoose.Schema({
    id: String,
    _id: { type: "string" },
    password: String,
    isactive: Boolean,
    firstlogin: Boolean,
    name: String,
    email: String,
    address: String,
    period: Date,
})

module.exports = mongoose.model('User', UserSchema)