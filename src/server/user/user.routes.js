module.exports = function(app) {
    var user = require("../user/user.cotroller.js");
    
    app.post('/adduser', user.create);
    app.get('/user', user.findAll);
    app.get('/user/:usrId', user.findOne);
    app.get('/login/:usrId/:usrPass', user.logIn);
    app.get('/maxuser', user.getMax);
    app.put('/user/:usrId', user.update);
    app.delete('/user/:usrId', user.delete);
    }
    