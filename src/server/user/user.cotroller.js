var User = require("../user/user.model.js");

exports.create = function(req, res) {

    var user = new User ({id: req.body.id, _id:req.body.id, password:req.body.id, 
                            isactive:req.body.isactive, name: req.body.name, address: req.body.address,
                            firstlogin:req.body.firstlogin, email:req.body.email, period:req.body.period  });

    user.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    User.find(function(err,user){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(user)
        }
    });
};

exports.findOne = function(req, res) {
    User.find({"_id": req.params.usrId}, function(err,user){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(user)
        }
    })
}

exports.getMax = function(req, res) {
    User.find(function(err,user){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(user)
        }
    }).sort({"_id":-1}).limit(1);
};

exports.logIn = function(req, res) {
    
    User.find({"id": req.params.usrId, "password": req.params.usrPass }, function(err,user){
        if(err) {
        res.status(500).send(err.errmsg)
        }else {
            res.send(user)
        }
    })
}

exports.update = function(req, res) {

    User.update ({"_id": req.params.usrId}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    User.remove({"_id": req.params.usrId}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}