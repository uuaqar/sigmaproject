var Unit = require("../unit/unit.model.js");

exports.create = function(req, res) {
    
    var unit = new Unit ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name });

    unit.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Unit.find(function(err,unit){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(unit)
        }
    });
};

exports.findOne = function(req, res) {
    Unit.find({"_id": req.params.unitCode}, function(err,unit){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(unit)
        }
    })
}

exports.getMax = function(req, res) {
    Unit.find().sort({"_id":-1}).limit(1).exec(function(err,unit){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(unit)
        }
    })
};

exports.update = function(req, res) {

    Unit.update ({"_id": req.params.unitCode}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Unit.remove({"_id": req.params.unitCode}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}