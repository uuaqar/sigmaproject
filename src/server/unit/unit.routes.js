module.exports = function(app) {
    var unit = require("../unit/unit.cotroller.js");
    
    app.post('/addunit', unit.create);
    app.get('/unit/:unitCode', unit.findOne);
    app.get('/unit', unit.findAll);
    app.get('/maxunit', unit.getMax);
    app.put('/unit/:unitCode', unit.update);
    app.delete('/unit/:unitCode', unit.delete);
    }
    