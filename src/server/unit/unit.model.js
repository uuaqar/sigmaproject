var mongoose = require("../../../node_modules/mongoose");

    var UnitSchema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String
})

module.exports = mongoose.model('Unit', UnitSchema)