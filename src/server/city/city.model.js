var mongoose = require("../../../node_modules/mongoose");

    var CitySchema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String,
    province: String,
})

module.exports = mongoose.model('City', CitySchema)