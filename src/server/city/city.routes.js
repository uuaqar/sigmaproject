module.exports = function(app) {
    var city = require("../city/city.cotroller.js");
    
    app.post('/addcity', city.create);
    app.get('/city/:cityCode', city.findOne);
    app.get('/city', city.findAll);
    app.get('/maxcity', city.getMax);
    app.put('/city/:cityCode', city.update);
    app.delete('/city/:cityCode', city.delete);
    }
    