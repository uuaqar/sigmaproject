var City = require("../city/city.model.js");

exports.create = function(req, res) {
    
    var city = new City ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name, province: req.body.province });

    city.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    City.find(function(err,city){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(city)
        }
    });
};

exports.findOne = function(req, res) {
    City.find({"_id": req.params.cityCode}, function(err,city){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(city)
        }
    })
}

exports.getMax = function(req, res) {
    City.find().sort({"_id":-1}).limit(1).exec(function(err,city){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(city)
        }
    })
};

exports.update = function(req, res) {

    City.update ({"_id": req.params.cityCode}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    City.remove({"_id": req.params.cityCode}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}