var Store = require("../store/store.model.js");

exports.create = function(req, res) {
    
    var store = new Store ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name, location: req.body.location });

    store.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Store.find(function(err,store){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(store)
        }
    });
};

exports.findOne = function(req, res) {
    Store.find({"_id": req.params.storeCode}, function(err,store){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(store)
        }
    })
}

exports.getMax = function(req, res) {
    Store.find().sort({"_id":-1}).limit(1).exec(function(err,store){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(store)
        }
    })
};

exports.update = function(req, res) {

    Store.update ({"_id": req.params.storeCode}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Store.remove({"_id": req.params.storeCode}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}