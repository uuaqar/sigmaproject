module.exports = function(app) {
    var store = require("../store/store.cotroller.js");
    
    app.post('/addstore', store.create);
    app.get('/store/:storeCode', store.findOne);
    app.get('/store', store.findAll);
    app.get('/maxstore', store.getMax);
    app.put('/store/:storeCode', store.update);
    app.delete('/store/:storeCode', store.delete);
    }
    