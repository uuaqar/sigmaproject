var mongoose = require("../../../node_modules/mongoose");

    var StoreSchema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String,
    location: String,
})

module.exports = mongoose.model('Store', StoreSchema)