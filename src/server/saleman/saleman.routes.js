module.exports = function(app) {
    var saleman = require("../saleman/saleman.cotroller.js");
    
    app.post('/addsaleman', saleman.create);
    app.get('/saleman', saleman.findAll);
    app.get('/saleman/:salemanCode', saleman.findOne);
    app.get('/maxsaleman', saleman.getMax);
    app.put('/saleman/:salemanCode', saleman.update);
    app.delete('/saleman/:salemanCode', saleman.delete);
   

    }
    