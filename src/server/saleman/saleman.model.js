
var mongoose = require("../../../node_modules/mongoose");
 
    var SalemanSchema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    name: String,
    isactive: Boolean,
    address: String,
    cnic: String,
    cell: String
})

module.exports = mongoose.model('Saleman', SalemanSchema)


