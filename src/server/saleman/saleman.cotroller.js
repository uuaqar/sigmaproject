var Saleman = require("../saleman/saleman.model.js");

exports.create = function(req, res) {
    
    var saleman = new Saleman ({code:req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name:req.body.name, address:req.body.address, cnic:req.body.cnic,
                            cell:req.body.cell
                           });

    saleman.save(function(err, data){
        // console.log(data);
        if(err){
            console.log(err);
            res.send(err.errmsg);
        } else {
            console.log(data);
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Saleman.find( function(err,saleman){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(saleman)
        }
    });
};

exports.getMax = function(req, res) {
    Saleman.find().sort({"_id":-1}).limit(1).exec(function(err,saleman){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(saleman)
        }
    })
};

exports.findOne = function(req, res) {
    Saleman.find({"_id": req.params.salemanCode }, function(err,saleman){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(saleman)
        }
    })
}

exports.update = function(req, res) {

    Saleman.update ({"_id": req.params.salemanCode}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Saleman.remove({"_id": req.params.salemanCode}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}
