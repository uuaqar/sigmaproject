var Iattrbt1 = require("../itemattribute1/itemattribute1.model.js");

exports.create = function(req, res) {
    
    var iattrbt1 = new Iattrbt1 ({code: req.body.code, _id:req.body.code, isactive:req.body.isactive, 
                            name: req.body.name });

    iattrbt1.save(function(err, data){
        console.log(data);
        if(err){
            // console.log(err);
            res.send(err.errmsg);
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Iattrbt1.find(function(err,iattrbt1){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt1)
        }
    });
};

exports.findOne = function(req, res) {
    Iattrbt1.find({"_id": req.params.iattrbt1Code}, function(err,iattrbt1){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt1)
        }
    })
}

exports.getMax = function(req, res) {
    Iattrbt1.find().sort({"_id":-1}).limit(1).exec(function(err,iattrbt1){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(iattrbt1)
        }
    })
};

exports.update = function(req, res) {

    Iattrbt1.update ({"_id": req.params.iattrbt1Code}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    Iattrbt1.remove({"_id": req.params.iattrbt1Code}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}