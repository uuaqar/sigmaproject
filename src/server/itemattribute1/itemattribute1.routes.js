module.exports = function(app) {
    var iattrbt1 = require("../itemattribute1/itemattribute1.cotroller.js");
    
    app.post('/addiattrbt1', iattrbt1.create);
    app.get('/iattrbt1/:iattrbt1Code', iattrbt1.findOne);
    app.get('/iattrbt1', iattrbt1.findAll);
    app.get('/maxiattrbt1', iattrbt1.getMax);
    app.put('/iattrbt1/:iattrbt1Code', iattrbt1.update);
    app.delete('/iattrbt1/:iattrbt1Code', iattrbt1.delete);
    }
    