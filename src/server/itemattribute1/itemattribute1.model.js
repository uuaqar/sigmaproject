var mongoose = require("../../../node_modules/mongoose");

    var Iattrbt1Schema = mongoose.Schema({
    code: String,
    _id: { type: "string" },
    isactive: Boolean,
    name: String
})

module.exports = mongoose.model('Iattrbt1', Iattrbt1Schema)