var mongoose = require("../../../node_modules/mongoose");
 
    var ChartOfAccSchema = mongoose.Schema({
    code: {type: "string", unique: true},
    title: String,
    isactive: Boolean,
    type: String,
    nature: String,
    
})

// ChartOfAccSchema.index({code: 1, type: 1}, {unique: true});

module.exports = mongoose.model('ChartOfAcc', ChartOfAccSchema)


