var ChartOfAcc = require("../chartofacc/chartofacc.model.js");

exports.create = function(req, res) {
    
    var chartofacc = new ChartOfAcc ({code:req.body.code, isactive:req.body.isactive, 
                            title:req.body.title, type:req.body.type,
                            nature:req.body.nature });

    chartofacc.save(function(err, data){
        // console.log(data);
        if(err){
            console.log(err);
            res.send(err.errmsg);
        } else {
            console.log(data);
            res.send(data);
        }
    });
};

exports.findAll = function(req, res) {
    ChartOfAcc.find(function(err,chartofacc){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(chartofacc)
        }
    });
};

exports.getMax = function(req, res) {
    ChartOfAcc.find().sort({"code":-1}).limit(1).exec(function(err,chartofacc){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(chartofacc)
        }
    })
};

exports.findOne = function(req, res) {
    ChartOfAcc.find({"code": req.params.Code}, function(err,chartofacc){
        if(err) {
        res.status(500).send(err.errmsg)
        } else {
            res.send(chartofacc)
        }
    })
}

exports.update = function(req, res) {

    ChartOfAcc.update ({"code": req.params.Code}, req.body, function(err,data){
        if(err){
            res.status(500).send(err.errmsg)
        } else {
            res.send(data)
        }
    })

}

exports.delete = function(req, res) {
    ChartOfAcc.remove({"code": req.params.Code}, function(err, data){
        if(err){
            // res.status(500).send(err.errmsg)
            res.status(500).send(err)
        } else {
            res.send(data)
        }
    } )
}