module.exports = function(app) {
    var chartofacc = require("../chartofacc/chartofacc.cotroller.js");
    
    app.post('/addchartofacc', chartofacc.create);
    app.get('/chartofacc', chartofacc.findAll);
    app.get('/chartofacc/:Code', chartofacc.findOne);
    app.get('/maxchartofacc', chartofacc.getMax);
    app.put('/chartofacc/:Code', chartofacc.update);
    app.delete('/chartofacc/:Code', chartofacc.delete);
    
    }
    